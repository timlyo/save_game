# Definitions
* Save - save game information
* Stored save - a save that has been copied from live to another directory
* Live save - save that is read and written from by the game
* Backup - The action of copying a save from a live directory

# Must
* Never change live saves unless explicitly specified
* Never change an already stored save
    * An edge case may arise for this, but at this stage in design none have been conceived
* Be able to detect game saves anywhere in the system hierarchy
    * Scan entire hierarchy via use of an operating system specific iterator
    * Linux is easy, simply scan from root
    * Windows will require combining all drives
* Be able to make copies of saves
* Be able to store multiple dated copies of saves
* Be able to differentiate between every recognised game and store saves in the correct directory
    * A database will contain directory patterns
    * A game is considered recognised if a directory name is recognised and a number of directories/files inside of it are recognised
    * Files and directories can be black listed so that even if a directory with the correct name is found, if it contains that file or directory then it will not be considered that game
* Expose a json api for all available commands and data
* Be able to identify a stored save by game and date
    * Date will be part of the name
* Be able to save an arbitrary number of saves without conflict
    * Stored save names will contain a has
* Track when a live save has changed
    * Inotify/${windows thing} will be used to watch live versions
    * Saves will be hashed and compared against stored saves to check for changes
* Keep saves of each game separate
    * Each game will have it's own directory to store saves

# Should
* Be able to restore a save to replace the live copy
* Save a list of installed games to speed startup and user experience
* A minimum interval for time between backups
* Allow a user to blacklist directories from scanning
* Allow a user to disable scanning for recognised games
* Allow a user to manually scan for recognised games
* Allow a user to choose to run on each startup

# Could
* Scan likely directories before less likely ones
    * e.g. scan steam folder early
