use std::fs::File;
use std::io::Result;
use std::io::Read;
use std::path::PathBuf;

pub fn read_file(name: PathBuf) -> Result<String> {
    let mut file = File::open(name)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}