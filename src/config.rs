use xdg::BaseDirectories;
use std::io;
use std::path::PathBuf;

use serde_yaml;
use serde_yaml::Value;
use std::collections::HashSet;

use util;

#[derive(Debug)]
pub enum ConfigErrors {
    io(io::Error),
    serde(serde_yaml::Error)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    excluded_directories: HashSet<String>
}

impl Config {
    /// Get relevant directory for platform
    #[cfg(unix)]
    pub fn get_save_location() -> io::Result<PathBuf> {
        let xdg_base = BaseDirectories::with_prefix("save_game").expect("Failed to get xdg");
        xdg_base.place_config_file("config.yaml")
    }

    /// Load config object from file
    pub fn load_config() -> Result<Config, ConfigErrors> {
        let location = Config::get_save_location().expect("Failed to get config location");

        // If no config file is found then return none and load default further up
        if !location.exists() {
            return Err(ConfigErrors::io(io::Error::new(io::ErrorKind::NotFound, "Config Doesn't exist")));
        }

        let content = match util::read_file(location) {
            Ok(content) => content,
            Err(e) => return Err(ConfigErrors::io(e))
        };

        match serde_yaml::from_str(&content) {
            Ok(content) => Ok(content),
            Err(e) => Err(ConfigErrors::serde(e))
        }
    }

    /// Returns true if a directory is excluded
    pub fn is_excluded(&self, directory: &str) -> bool{
        self.excluded_directories.contains(directory)
    }
}

impl Default for Config {
    fn default() -> Config {
        let mut excluded = HashSet::new();
        excluded.insert("/bin".to_string());
        excluded.insert("/proc".to_string());
        excluded.insert("/srv".to_string());

        Config {
            excluded_directories: excluded
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_from_disk() {
        let config = Config::load_config();
        println!("{:?}", config);
        assert!(config.is_ok())
    }

    #[test]
    fn test_is_excluded() {
        let config = Config::default();
        assert!(config.is_excluded("/bin"))
    }
}
