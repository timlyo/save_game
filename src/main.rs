extern crate xdg;
extern crate serde_yaml;
extern crate walkdir;

#[macro_use]
extern crate serde_derive;

mod system;
mod config;
mod util;

fn main() {
    let config = config::Config::load_config();
    println!("Hello, world!");
}
