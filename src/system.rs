use walkdir;
use config::Config;
use walkdir::{DirEntry, WalkDirIterator, Iter};
use std::iter::FilterMap;

/// return all directories on the system
pub fn get_all_unexcluded_directories(config: &Config) -> Vec<DirEntry> {
    let walker: Iter = walkdir::WalkDir::new("/").into_iter();
    walker.filter_entry(|e| {
        let name = match e.file_name().to_str() {
            Some(name) => name,
            None => {
                println!("Error, none unicode filename: {:?}", e);
                return false;
            }
        };

        config.is_excluded(name) == false
    }).filter_map(|e| e.ok()).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn test_get_all_directories() {
        let config = Config::default();
        let result = get_all_unexcluded_directories(&config);
        let amount = result.into_iter().count();
        println!("{}", amount);
    }
}
